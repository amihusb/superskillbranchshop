package com.skillbranch.bestshop.mvp.presenters;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.fernandocejas.frodo.annotation.RxLogSubscriber;
import com.skillbranch.bestshop.data.storage.dto.ActivityPermissionsResultDto;
import com.skillbranch.bestshop.data.storage.dto.ActivityResultDto;
import com.skillbranch.bestshop.data.storage.dto.UserInfoDto;
import com.skillbranch.bestshop.mvp.models.AccountModel;
import com.skillbranch.bestshop.mvp.models.RootModel;
import com.skillbranch.bestshop.mvp.views.IRootView;
import com.skillbranch.bestshop.ui.activities.RootActivity;
import com.skillbranch.bestshop.ui.activities.SplashActivity;
import com.skillbranch.bestshop.utils.AvatarHelper;
import com.skillbranch.bestshop.utils.MvpAuthApplication;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mortar.Presenter;
import mortar.bundler.BundleService;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

import static com.skillbranch.bestshop.utils.ConstantManager.FILE_PROVIDER_AUTHORITY;
import static com.skillbranch.bestshop.utils.ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA;
import static com.skillbranch.bestshop.utils.ConstantManager.REQUEST_PROFILE_PHOTO_GALLERY;

public class RootPresenter extends Presenter<IRootView> {
    private static final String TAG = RootPresenter.class.getSimpleName()
            ;
    @Inject
    RootModel mRootModel;
    @Inject
    AccountModel mAccountModel;

    private Subscription userInfoSub;

    private static int DEFAULT_MODE = 0;
    private static int TAB_MODE = 1;

    private String mPhotoFileUrl;

    private BehaviorSubject<ActivityResultDto> mActivityResultSubject = BehaviorSubject.create();
    private BehaviorSubject<ActivityPermissionsResultDto> mActivityPermissionsResultSubject = BehaviorSubject.create();

    //region ================= Getters for BehaviorSubjects =================

    public BehaviorSubject<ActivityResultDto> getActivityResultSubject() {
        return mActivityResultSubject;
    }

    public BehaviorSubject<ActivityPermissionsResultDto> getActivityPermissionsResultSubject() {
        return mActivityPermissionsResultSubject;
    }

    //endregion

    public RootPresenter() {
        MvpAuthApplication.getRootActivityRootComponent().inject(this);
    }

    @Override
    protected BundleService extractBundleService(IRootView view) {
        return (view instanceof RootActivity) ?
                BundleService.getBundleService((RootActivity) view) :  //привязываем RootPresenter к RootActivity
                BundleService.getBundleService((SplashActivity) view); //привязываем RootPresenter к SplashActivity
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);

        if(getView() instanceof RootActivity) {
            userInfoSub = subscribeOnUserInfoObs();
        }
    }

    @Override
    public void dropView(IRootView view) {
        if(userInfoSub != null) {
            userInfoSub.unsubscribe();
        }
        super.dropView(view);
    }

    private Subscription subscribeOnUserInfoObs() {
        return mAccountModel.getUserInfoObs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new UserInfoSubscriber());
    }

//    public int getCartProductCount() {
//        return mRootModel.loadCartProductCounter();
//    }
//
//    public void setCartProductCount(int cartProductCount) {
//        mRootModel.saveCartProductCounter(cartProductCount);
//    }

    @Nullable
    public IRootView getRootView() {
        return getView();
    }

    public ActionBarBuilder newActionBarBuilder() {
        return this.new ActionBarBuilder();
    }

    @RxLogSubscriber
    private class UserInfoSubscriber extends Subscriber<UserInfoDto> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            if (getView() != null) {
                getView().showError(e);
            }
        }

        @Override
        public void onNext(UserInfoDto userInfoDto) {
            if (getView() != null) {
                getView().initDrawer(userInfoDto);
            }
        }
    }

    //region ================= Permissions =================

    public void startActivityForResult(Intent intent, int requestCode) {
        ((RootActivity) getView()).startActivityForResult(intent, requestCode);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult requestCode=" + requestCode + " resultCode=" + resultCode);
        mActivityResultSubject.onNext(new ActivityResultDto(requestCode, resultCode, data));
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mActivityPermissionsResultSubject.onNext(new ActivityPermissionsResultDto(requestCode, permissions, grantResults));
    }

    public boolean checkPermissions(@NonNull String[] permissions) {
        boolean allGranted = true;
        for (String permission : permissions) {
            int selfPermission = ContextCompat.checkSelfPermission(((RootActivity) getView()), permission);
            if (selfPermission != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }
        return allGranted;
    }

    public boolean requestPermissions(@NonNull String[] permissions, int requestCode) {
        boolean isRequested = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ((RootActivity) getView()).requestPermissions(permissions, requestCode);
            isRequested = true;
        }
        return isRequested;
    }

    //endregion

    public Observable<String> getActivityResultPublishUrlSubject() {
        return getActivityResultSubject()
                .filter(res -> res.getResultCode() == Activity.RESULT_OK
                        && (res.getRequestCode() == REQUEST_PROFILE_PHOTO_GALLERY
                        || res.getRequestCode() == REQUEST_PROFILE_PHOTO_CAMERA))
                .map(res -> (res.getIntent() == null || res.getIntent().getData() == null ?
                        mPhotoFileUrl :
                        res.getIntent().getData().toString()))
                .filter(uri -> uri != null);
    }

    public Uri createFileForPhoto() {
        File file = AvatarHelper.createFileForPhoto();

        mPhotoFileUrl = Uri.fromFile(file).toString();

        return FileProvider.getUriForFile((Activity) getView(),
                FILE_PROVIDER_AUTHORITY,
                file);
    }

    //region ================= Check Permissions =================

    public boolean checkPermissionsAndRequestIfNotGranted(@NonNull String[] permissions, int requestCode) {
        boolean allGranted = true;
        for (String permission : permissions) {
            int selfPermission = ContextCompat.checkSelfPermission((RootActivity) getView(), permission);
            if (selfPermission != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }

        if (!allGranted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ((RootActivity) getView()).requestPermissions(permissions, requestCode);
            }
            return false;
        }
        return allGranted;
    }

    //endregion

    public class ActionBarBuilder {
        private boolean isGoBack = false;
        private boolean isVisible = true;
        private CharSequence title;
        private List<MenuItemHolder> items = new ArrayList<>();
        private ViewPager pager;
        private int toolbarMode = DEFAULT_MODE;

        public ActionBarBuilder setBackArrow(boolean enable) {
            this.isGoBack = enable;
            return this;
        }

        public ActionBarBuilder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public ActionBarBuilder addAction(MenuItemHolder menuItem) {
            this.items.add(menuItem);
            return this;
        }

        public ActionBarBuilder setTab(ViewPager pager) {
            this.toolbarMode = TAB_MODE;
            this.pager = pager;
            return this;
        }

        public void build() {
            if(getView() != null) {
                RootActivity activity = (RootActivity) getView();
                activity.setVisible(isVisible);
                activity.setTitle(title);
                activity.setBackArrow(isGoBack);
                activity.setMenuItem(items);
                if(toolbarMode == TAB_MODE) {
                    activity.setTabLayout(pager);
                } else {
                    activity.removeTabLayout();
                }
            }
        }
    }
}
